import React, {Component} from 'react';
import Board from './Board'
import calculateWinner from '../service/tictactoeService'

class Game extends Component {

    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null)
            }],
            xIsNext: true,
            winner: null,
            stepNo: 0,
        }
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNo];

        const moves = history.map((step, move) => {
            const descr = move ? 'Go to move #' + move : 'Go to game start';

            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{descr}</button>
                </li>
            )
        });

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div><h1>{this.getStatus()}</h1></div>
                    <div>
                        <ol>{moves}</ol>
                    </div>
                </div>
            </div>
        );
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNo + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        if (!squares[i] && !this.state.winner) {
            squares[i] = this.getPlayer()
            const winner = calculateWinner(squares)

            this.setState({
                history: history.concat(
                    [{squares: squares}]
                ),
                xIsNext: !this.state.xIsNext,
                stepNo: history.length,
                winner: winner,
            })
        }
    }

    jumpTo(step) {
        this.setState({
            stepNo: step,
            xIsNext: (step % 2) === 0,
            winner: null,
        });
    }

    getStatus() {
        if (this.state.winner) {
            return 'Winner: ' + this.state.winner;
        } else {
            if (this.state.stepNo === 9) {
                return 'DRAW'
            } else {
                return 'Next player: ' + this.getPlayer();
            }
        }
    }

    getPlayer() {
        return this.state.xIsNext ? 'X' : 'O'
    }
}

export default Game;
